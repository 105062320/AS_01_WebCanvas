
!function () {
    String.prototype.reverse2 = function () {
        var newstr = "";
        for (var i = this.length-1; i >= 0; i-=2) {
            newstr += this[i-1] + this[i];
        } 
        return newstr;
    }


    function point (x, y) {
        this.x = x;
        this.y = y;
    }


    function colorInit () {
        for (var i = 0; i < initColors.length; i++){
            var newColorSelector = $('<li></li>');
            newColorSelector.css('background-color', initColors[i]);
            $('#controls ul').append(newColorSelector);

            if (i === 0) {
                newColorSelector.addClass('selected');; 
            }
        }
    }


    function changeColorAndOutput (boo) {
        var r = $('#red').val();
        var g = $('#green').val();
        var b = $('#blue').val();
        var a = ($('#alphac').val() / 100) * 255;
        var rgba = 'rgba(' + r + ',' + g + ',' + b + ',' + ($('#alphac').val() / 100).toFixed(2) + ')';
        var hex =  (256 + parseInt(r)).toString(16).substr(1) + ((1 << 24) + (parseInt(g) << 16) | (parseInt(b) << 8) | parseInt(a)).toString(16).substr(1);
        var kml = hex.reverse2();
        $('#newColor').css('background-color', rgba);
        $('#curcolorRGBA').html(rgba);
        $('#curcolorHEX').html("#" + hex);
        $('#curcolorKML').html("#" + kml);
        if (boo) {
            var el = $(this);
            el.next('output').val(el.val());
        }
    }

    function getAlphaAsRGB () {
        var sourceColors = $('#newColor').css('background-color').replace(/[rgba()]/g, '').split(',');
        var srca = parseFloat(sourceColors[3]);
        for (var i = 0; i < sourceColors.length; i++) {
            sourceColors[i] = parseFloat(sourceColors[i]) / 255;
        }
        var tred = Math.round(((1 - srca) + (srca * sourceColors[0])) * 255);
        var tgreen = Math.round(((1 - srca) + (srca * sourceColors[1])) * 255);
        var tblue = Math.round(((1 - srca) + (srca * sourceColors[2])) * 255);
        return 'rgb(' + tred + ',' + tgreen + ',' + tblue + ')';
    }


    function addRestorePoint () {
        var dataURL = document.getElementById('canvas').toDataURL('image/png');
        undoPoints.push(dataURL);
    }

 
    function updateImg () {
        context.drawImage(tmpCanvas[0], 0, 0);
        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);
    }

 
    function getRandomOffset (r) {
        return {
            x: Math.cos(Math.random() * (2 * Math.PI)) * (Math.random() * r),
            y: Math.sin(Math.random() * (2 * Math.PI)) * (Math.random() * r)
        };
    } 
 
  
    function plotStarWithSize (multi) {
        tmpContext.moveTo(tool.startx + starPoints[0].x * multi, tool.starty + starPoints[0].y * multi);
        for (var i=1; i<starPoints.length; i++) {
            tmpContext.lineTo(tool.startx + starPoints[i].x * multi, tool.starty + starPoints[i].y * multi);
        }
        tmpContext.closePath();
    }
    

    

    var canvasWidth = screen.availWidth * 0.5;
    var canvasHeight = screen.availHeight * 0.7;

    $("#container").append("<canvas width='" + canvasWidth + "' height='" + canvasHeight + "' id='canvas'></canvas>");
    var canvas = $('#canvas');
    canvas.css('margin-left', '-' + (canvasWidth / 2) + 'px');
    var context = canvas[0].getContext('2d');

    $("#container").append("<canvas width='" + canvasWidth + "' height='" + canvasHeight + "' id='tempCanvas'></canvas>");

    var tmpCanvas = $('#tempCanvas');
    tmpCanvas.css('margin-left', '-' + (canvasWidth / 2) + 'px');

    var tmpContext = tmpCanvas[0].getContext('2d');
 
    var lastEvent, sprayInterval,  mouseDown = false, drawn = false, redoActive = false, undoActive = false, dropper = false, undoPoints = [], redoPoints = [], tool = {}, color;

    var starPoints = [
        new point(0,-0.9),
        new point(-0.3,-0.25),
        //new point(-1,-0.25),//----------
        //new point(-0.4,0.1),//-----------
        new point(-0.8,0.9),
        //new point(0,0.35),//--------------
        new point(0.8,0.9),
        //new point(0.4,0.1),//-----------
        //new point(1,-0.25),//-------------
        new point(0.3,-0.25),
    ];
    //初始化顏色
    var initColors = ['black', 'white', 'red', 'orange', 'yellow', 'green', 'blue', 'DarkBlue', 'Indigo', 'purple'];

    tool.mode = 'pen';
    tool.lineWidth = '1';

    tmpContext.globalCompositeOperation = 'source-over'; 
    tmpContext.shadowBlur = 1;
    tmpContext.lineCap = 'round';
    tmpContext.lineJoin = 'round';
    context.textBaseline = 'middle';



    
    $('.copytext').click(function() {
        var span = this;
        var range = document.createRange();
        range.setStartBefore(span.firstChild);
        range.setEndAfter(span.lastChild);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    });
    

    //When clicking on control list items
    $('#controls').on('click', 'li', function () {
        

            $(this).siblings().removeClass('selected');
            $(this).siblings().removeClass('selectedLight');

            if ($(this)[0].hasAttribute('data-opacity')) {
                color = getAlphaAsRGB();
            }
            else {
                color = $(this).css('background-color');
            }
            
            var thisbgcolors = $(this).css('background-color').replace(/[rgb()]/g,'').split(',');
            if (parseInt(thisbgcolors[0]) > 175 && parseInt(thisbgcolors[1]) > 175 && parseInt(thisbgcolors[2]) > 175) {
                $(this).addClass('selectedLight');
            }
            else {

                $(this).addClass('selected');
            }
        
    });
    var font_name ='Arial';
    $('#inputGroupSelect01').change(function () {
        font_name= $('#inputGroupSelect01').val();
    });
    
    //Choose different tools
    $('#toolBelt').change(function () {
        var tval = $('#toolBelt').val();
        tool.mode = tval;
        if(tool.mode=="pen"){
            $('canvas').css('cursor', 'url(images/PencilToolCursor.gif), crosshair');
        }else if(tool.mode=="line"){
            $('canvas').css('cursor', 'url(images/' + tval + '.png), crosshair');
        }else if(tool.mode=="textOutline"){
            $('canvas').css('cursor', 'url(images/Textcursor.png), crosshair');
        }else if(tool.mode=="textFill"){
            $('canvas').css('cursor', 'url(images/Textcursor.png), crosshair');
        }else if(tool.mode=="eraser"){
            $('canvas').css('cursor', 'url(images/EraserTool.png), crosshair');
        }else if(tool.mode=="spray"){
            $('canvas').css('cursor', 'url(images/Spraycursor.png), crosshair');
        }else if(tool.mode=="rectOutline"||tool.mode=="rectFill"){
            $('canvas').css('cursor', 'url(images/' + tval + '.png), crosshair');
        }else if(tool.mode=="circleOutline"||tool.mode=="circleFilll"){
            $('canvas').css('cursor', 'url(images/' + tval + '.png), crosshair');
        }else{
            $('canvas').css('cursor', 'url(images/' + tval + '.png), crosshair');
        }
       
        if(tool.mode == "textOutline"||tool.mode == "textFill"){
            $('#font-select').css("visibility", "visible");
        }else{
            $('#font-select').css("visibility", "hidden");
        }
    });

    
    //Choose different line widths
    $('#toolSize').change(function () {
        tool.lineWidth = $('#toolSize option:selected').val();
    });

    //Clear the canvas after confirmation
    $('#clearSketch').click(function () {
        
           

                context.clearRect(0, 0, canvas[0].width, canvas[0].height);

                redoPoints = [];
                undoPoints = [];
                var dataURL = document.getElementById('canvas').toDataURL('image/png');
                undoPoints.push(dataURL);
            
        
    });

    
    //When color sliders change
    $('input[type=range]').on('input', changeColorAndOutput);


    tmpCanvas.mousedown(function (e) {
        

            lastEvent = e;

            tool.startx = e.pageX-tmpCanvas.offset().left;
            tool.starty = e.pageY-tmpCanvas.offset().top;

            tmpContext.strokeStyle = color;
            tmpContext.fillStyle = color;
            context.strokeStyle = color;
            context.fillStyle = color;
            tmpContext.shadowColor = color; 

            tmpContext.lineWidth = tool.lineWidth;
            mouseDown = true;

            if (tool.mode === 'starFill' || tool.mode === 'starOutline') {
                plotStarWithSize(1);
                if (tool.mode === 'starFill') {
                    tmpContext.fill(); 
                }
                else {
                    tmpContext.stroke();
                }
                drawn = true;    
            }
            else if (tool.mode === 'textFill' || tool.mode === 'textOutline') {
                    var $input = $('#textInput');
                    $input.css({
                        width:370,
                        display: 'block',
                        position : 'absolute',
                        left : tool.startx ,
                        top : tool.starty
                    });
                    $input.keyup(function (e) {
                        if (e.which === 13) {
                            e.preventDefault();
                            context.font = (10 + (tmpContext.lineWidth * 3)) +'px ' + font_name;
                            if (tool.mode === 'textFill') {
                                //left=left-400;
                                context.fillText($input.val(), parseInt($input.css('left')), parseInt($input.css('top')));
                            } 
                            else {
                                //left=left-400;
                                context.strokeText($input.val(), parseInt($input.css('left')), parseInt($input.css('top')));
                            }
                            context.save();
                            addRestorePoint();
                            $input.css('display', 'none').val('');
                        }
                        if (e.which === 27) {
                            e.preventDefault();
                            $input.css('display', 'none').val('');          
                        }                
                    });

            }   
            context.save();
        
    }).mousemove(function (e){
        
            //Draw lines
            if (mouseDown) {
                tmpContext.beginPath();
                var lastEventposX = lastEvent.pageX-canvas.offset().left;
                var lastEventposY = lastEvent.pageY-canvas.offset().top;
                var xpos = e.pageX-canvas.offset().left;
                var ypos = e.pageY-canvas.offset().top;
                switch (tool.mode) {
                    case 'pen':      
                        tmpContext.moveTo(lastEventposX, lastEventposY);
                        tmpContext.lineTo(xpos, ypos);
                        tmpContext.stroke();     
                        break;
                    case 'line':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);  
                        tmpContext.moveTo(tool.startx, tool.starty);
                        tmpContext.lineTo(xpos, ypos);
                        tmpContext.stroke();
                        break;
                    case 'eraser':
                        context.globalCompositeOperation = 'destination-out';
                        context.rect(xpos, ypos, 20, 20);
                        context.fill();
                        break;
                    case 'rectOutline':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);                
                        tmpContext.strokeRect(Math.min(xpos, tool.startx), Math.min(ypos, tool.starty), Math.abs(xpos-tool.startx), Math.abs(ypos-tool.starty)); 
                        break;
                    case 'rectFill':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);                
                        tmpContext.fillRect(Math.min(xpos, tool.startx), Math.min(ypos, tool.starty), Math.abs(xpos-tool.startx), Math.abs(ypos-tool.starty)); 
                        break;                
                    case 'circleOutline':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);
                        tmpContext.arc(tool.startx, tool.starty, Math.abs(tool.startx-xpos), 0, Math.PI * 2);
                        tmpContext.stroke();
                        break;
                    case 'circleFill':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height);
                        tmpContext.arc(tool.startx, tool.starty, Math.abs(tool.startx-xpos), 0, Math.PI * 2);
                        tmpContext.fill();
                        break;                
                    case 'spray':
                        var density = 35;
                        for (var i = 0; i < density; i++) {
                            var offset = getRandomOffset(10);
                            var x = xpos + offset.x;
                            var y = ypos + offset.y;
                            tmpContext.fillRect(x, y, 1, 1);
                        }
                        break;
                    case 'starFill':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height); 
                        plotStarWithSize(xpos-tool.startx);
                        tmpContext.fill();
                        break;            
                    case 'starOutline':
                        tmpContext.clearRect(0, 0, tmpCanvas[0].width, tmpCanvas[0].height); 
                        plotStarWithSize(xpos-tool.startx);
                        tmpContext.stroke();
                        break; 
                }
                drawn = true;
                lastEvent = e;     
          }
        
    }).mouseup(function () {
        
            mouseDown = false;
            tmpContext.closePath();
            context.restore();
            updateImg();
            if (drawn) {
                addRestorePoint();
            }
            drawn = false;
            dropper = true;
        
    }).mouseleave(function () {
        
            tmpContext.closePath();
            context.restore();
            updateImg();
            if (mouseDown && drawn) {
                addRestorePoint();
            }
            mouseDown = false;
            drawn = false;
            dropper = true;
        
    });



    colorInit();

    addRestorePoint();

    color = $('.selected').css('background-color');



    
    $('body').on('keydown', function (e) {
        //Undo function 
        if (e.ctrlKey) {
            if (undoPoints.length > 0) {
            var pimg = new Image();
            if (redoActive === false) {
                redoPoints.push(undoPoints.pop());
            }        
            pimg.onload = function () {
                redoPoints.push(pimg.src);
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                context.drawImage(pimg, 0, 0);
                redoActive = true;   
            }
            pimg.src = undoPoints.pop();
            }
            else{
                undoActive = false;
            }
        }
        //Redo function 
        if (e.shiftKey) {
            if (redoPoints.length > 0) {
            var pimg = new Image();
            if (undoActive === false) {
                undoPoints.push(redoPoints.pop());    
            }  
            pimg.onload = function () {
                undoPoints.push(pimg.src);
                context.clearRect(0, 0, canvas[0].width, canvas[0].height);
                context.drawImage(pimg, 0, 0);
                undoActive = true;
            }
            pimg.src = redoPoints.pop();  
            }
            else{
                redoActive = false;
            }
        }
    });

    window.onbeforeunload = function () {
        return 'Do you really want to leave/refresh the page?';
    }
}();

function save(){
    var dataurl = document.getElementById('canvas').toDataURL('image/png');
            var a = document.createElement('a');
            a.href = dataurl;
		    a.download = "canvas下載圖";
		    a.click();
};


document.getElementById("file").addEventListener('change',function(){
    //filelist
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var reader = new FileReader();
    var img_upload = new Image();
    if(this.files && this.files[0]){
      reader.addEventListener('load',function(event){
        img_upload.addEventListener('load',function(){
          ctx.drawImage(img_upload,0,0);
          url = img_upload.src;
          push_url();
          console.log(url_array[0]);
          console.log(url_array[1]);
          console.log(url_array.length);
          console.log(url_n);
        });
        img_upload.src = event.target.result;
      });
      reader.readAsDataURL(this.files[0]);
    }
    console.log(url_array[0]);
    console.log(url_array[1]);
    console.log(url_array.length);
    console.log(url_n);
  });